import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Book } from 'src/app/module/book';
import { BooksService } from 'src/app/service/books.service';




@Component({
  selector: 'app-listbooks',
  templateUrl: './listbooks.component.html',
  styleUrls: ['./listbooks.component.css']
})
export class ListbooksComponent implements OnInit {

  private book: Book[] = [];
  constructor(  private bookservice: BooksService,
                private router: Router) { }

  ngOnInit() {
      this.bookservice.getBooks().subscribe(data => {
      this.book = data;
      this.book.sort(function (a, b) {
                  return a.id - b.id;
      });
    });
  }

  delete(id, i) {
    this.bookservice.deleteBook(id).subscribe(() =>
    this.book.splice(i, 1));
  }

  edit(book) {
    this.router.navigate(['/editBook', book.id]);
  }

  showDetails(book) {
    this.router.navigate(['/detailBook', book.id]);
  }

}
