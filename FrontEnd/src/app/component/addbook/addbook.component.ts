import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { BooksService } from 'src/app/service/books.service';
import { Book } from 'src/app/module/book';


@Component({
  selector: 'app-addbook',
  templateUrl: './addbook.component.html',
  styleUrls: ['./addbook.component.css']
})
export class AddbookComponent {

  constructor(private books: BooksService,
    private router: Router) { }


  onClickSubmit(data) {
    // if (data.isbnNo === '' ||
    //   data.title === '' ||
    //   data.author === '' ||
    //   data.desc === '') {
    //   this.nija = false;
    // } else {
      this.books.addBook(data).subscribe(() => {
        this.router.navigate(['/listBooks']);
      });
    // }

  }
}
