import { Injectable, Injector, ErrorHandler } from '@angular/core';
import { Router } from '@angular/router';
import { isFormattedError } from '@angular/compiler';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {

  constructor(private injectotr: Injector) { }
  handleError(error: any  ) {
    const router = this.injectotr.get(Router);
    console.log(`Request URL : ${router.url}`);

    if (error instanceof HttpErrorResponse) {
        console.error('Backend returned status code : ', error.status);
        console.error('Response Body :', error.message);
    } else {
        console.log('An Error Occured : ', error.message);
    }
      router.navigate(['error']);
  }
}
