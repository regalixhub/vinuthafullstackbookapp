import { Injectable } from '@angular/core';
import { HttpClient , HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';

import { Book } from '../module/book';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  private baseUrl = 'http://10.250.12.230:8080/books/';
  private headers = new HttpHeaders({'Content-type': 'application/json'});
  private options = {headers : this.headers};

  constructor(private http: HttpClient) { }

  getBooks(): Observable<Book[]> {
    return this.http.get<Book[]>(this.baseUrl + '/', this.options);
  }

  addBook(data): Observable<Book> {
    return this.http.post<Book>(this.baseUrl, JSON.stringify(data), this.options);
  }

  getBookById(id: number): Observable<Book> {
    return this.http.get<Book>(this.baseUrl + '/' + id, this.options);
  }

  deleteBook(id: number) {
    return this.http.delete(this.baseUrl + id, this.options);
  }

  editBook(book: Book): Observable<Book> {
    return this.http.put<Book>(this.baseUrl + '/' + book.id, JSON.stringify(book), this.options);
  }
}
